/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examened;

/**
 *
 * @author kevin
 */
class Pilagenerica <Tipo>{
   private int tammax;
   private Tipo pila[];
   private int datos;
   
   public Pilagenerica(int n){
       tammax=n;
       datos=0;
       pila=(Tipo[]) new Object[tammax];
   }
   void agregarDatos(Tipo data)throws Exception{
       if(datos<tammax){
           pila[datos++]=data;
       }
   }
   Tipo elimdat()throws Exception{
       if(!pilavacia()){
           return pila[--datos];
       }
       throw new Exception("la pila esta vacia");
   }
   int numerodatospila(){
       return datos;
   }
   private boolean pilavacia() {
        return datos==0;
    }
   Tipo cimapila()throws Exception{
       if(pilavacia()){
           throw new Exception("la ila esta vacia");
       }
       int NumEnteExist=datos;
       return pila[--NumEnteExist];
   }
    /**
     * @return the tammax
     */
    public int getTammax() {
        return tammax;
    }

    /**
     * @param tammax the tammax to set
     */
    public void setTammax(int tammax) {
        this.tammax = tammax;
    }

    /**
     * @return the pila
     */
    public Tipo[] getPila() {
        return pila;
    }

    /**
     * @param pila the pila to set
     */
    public void setPila(Tipo[] pila) {
        this.pila = pila;
    }

    /**
     * @return the datos
     */
    public int getDatos() {
        return datos;
    }

    /**
     * @param datos the datos to set
     */
    public void setDatos(int datos) {
        this.datos = datos;
    }
}
