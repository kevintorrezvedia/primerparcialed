/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examened;

/**
 *
 * @author Kevin Angel Torrez Vedia   //8514889
 */
public class Pilacaracteres {
    char arr[];
    int top;
    
    Pilacaracteres(){
        arr=new char[10];
        top=-1;
    }
    public boolean vacio(){
        return(top==-1);
    }
    public void intro(char e){
        top++;
        if(top<arr.length){
            arr[top]=e;
        }else{
            char t[]= new char[arr.length+5];
            for(int i = 0; i < arr.length; i++){
                t[i]=arr[i];
            }
            arr=t;
            arr[top]=e;
        }
    }
    public char sacar(){
        return arr[top];
    }
    public char sdelete(){
        if(!vacio()){
            int ttop=top;
            top--;
            char returntop = arr[ttop];
            return returntop;
        }else{
            System.out.println(" Pila vacia ");
            return '-';
        }
    }
}

